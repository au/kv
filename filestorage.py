import os
import os.path
import urllib2


class FileStorage:
    def __init__(self, root=None):
        if root:
            self.root = root
        else:
            storepath = os.environ.get('KV_FILE_STORE')
            if storepath:
                self.root = os.path.expanduser(storepath)
            else:
                self.root = os.path.expanduser('~/.kv')

    def list(self, part):
        find = part
        if not part:
            find = ''
        filenames = []
        try:
            filenames = [self._rev_key(f) for f in os.listdir(self.root) if
                    os.path.isfile(os.path.join(self.root, f))]
        except:
            pass
        res = [f for f in filenames if find in f]
        res.sort()
        return res

    def remove(self, key):
        try:
            os.remove(os.path.join(self.root, self._key(key)))
        except:
            pass

    def save(self, key, value):
        if not os.path.isdir(self.root):
            try:
                os.makedirs(self.root)
            except:
                print "Can't create directory: %s" % (self.root)
                raise
        with open(os.path.join(self.root, self._key(key)), 'w') as f:
            f.write(value)

    def rename(self, key, newkey):
        val = self.load(key)
        self.save(newkey, val)
        self.remove(key)

    def load(self, key):
        value = ''
        try:
            with open(os.path.join(self.root, self._key(key)), 'r') as f:
                value = f.read()
        except:
            pass
        return value

    def _key(self, key):
        return key.replace('%', '%%').replace('/', '%s')

    def _rev_key(self, key):
        return key.replace('%s', '/').replace('%%', '%')

