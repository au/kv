_kv() 
{
  local cur prev opts
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"
  prev="${COMP_WORDS[COMP_CWORD-1]}"

  local keys=$(kv -l)

  case "${prev}" in
    -r | --remove | -p | -n | --rename)
      COMPREPLY=( $(compgen -W "${keys}" -- "${cur}") )
      return 0
      ;;
    *)
      COMPREPLY=( $(compgen -W "${keys} -h -r -l -n -p --rename --remove --list --help" -- "${cur}") )
      return 0
      ;;
  esac
}
complete -F _kv kv
